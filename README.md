Based on
https://github.com/jzwolak/Flask-Tus.git
however I've removed the default hard 4g Tus-Max-Size limit, in hopes that this will play nice with uppy's large file uploads





# Flask-Tus
Flask Extension implementing the Tus.io server protocol


## Prerequisites (redis)

Currently flask-tus is reliant on a local redis server.  This is used for caching information about
uploads in progress.  It is on the roadmap to remove this dependancy.  You must install the redis python package
for this extension to work.

```
pip install redis
```

## Installation

Installation from source (this repository)

```
python setup.py install
```

Installation from PyPi repository (recommended for latest stable release)

```
pip install Flask-Tus
```

## Usage

### demo.py

```python
from flask import Flask, render_template, send_from_directory
from flask_tus import tus_manager
import os

app = Flask(__name__)
tm = tus_manager(app, upload_url='/file-upload', upload_folder='uploads/')
```

tus_manager() registers two new url endpoint /file-upload and /file-upload/\<resource\>.  You can not define views for those
urls in your app.  Use any tus client and point it to  /file-upload as the endpoint

